# frozen_string_literal: true

require 'wtflogger/scribe'
require 'tempfile'

# temp logfile since for some reason I have not figured out yet
# I get a syslog alredy open error when using syslog in rspec

tmplog = Tempfile.new(['rspec-script', '.log'])

RSpec.describe Wtflogger::Scribe do
  %w{file}.each do |type| # %w{file syslog}
    context "scribing to #{type}" do
      let(:scribe_file) do
        described_class.new(
          :caller => File.basename(__FILE__),
          :level => :warn,
          :screen => true,
          :type => type,
          :logfile => tmplog.path
        )
      end

      it 'creates scribe with file output' do
        expect(scribe_file).not_to be_nil
      end

      it 'scribes a debug message' do
        rtn = scribe_file.debug('this is a debug message')
        expect(rtn).to be false
      end

      it 'scribes a info message' do
        rtn = scribe_file.info('this is a info message')
        expect(rtn).to be false
      end

      it 'scribes a warn message' do
        rtn = scribe_file.warn('this is a warn message')
        expect(rtn).to be true
      end
    end
  end
end
