# frozen_string_literal: true

# more core and stlibs

require 'logging'

module Wtflogger
  #
  # @class Scribe
  class Scribe
    # rubocop:disable Metrics/ParameterLists
    def initialize(
      caller: 'wtf-unknown',
      level: :warn,
      screen: false,
      type: 'journal',
      group: 'wtfo',
      logfile: '/dev/null'
    )
      #    Logging.init :debug, :info, :warn, :error, :fatal
      @log = Logging::Logger[caller]
      @log.level = validate_level(level)
      @log = Logging.logger[caller]

      if type == 'journal'
        type = 'syslog' unless File.directory?('/run/systemd/journal') # rubocop:disable Style/SoleNestedConditional
      end

      @log.add_appenders(appender(caller, type, group, logfile, screen))
      @log.add_appenders(Logging.appenders.stdout) if screen
    end
    # rubocop:enable Metrics/ParameterLists

    def debug(message)
      @log.debug message
    end

    def info(message)
      @log.info message
    end

    def warn(message)
      @log.warn message
    end

    def error(message)
      @log.error message
    end

    def fatal(message)
      @log.fatal message
    end

    def unknown(message)
      @log.unknown message
    end

    private

    def validate_level(level)
      level.class == 'Symbol' ? level : level.to_sym
    end

    def appender(caller, type, group, logfile, screen)
      case type
      when 'journal'
        result = Logging.appenders.journald(
          caller, :layout => Logging.layouts.pattern(:pattern => "%m\n"), # optional layout
                  :ndc => false, # log ndc hash values into custom journal fields (true by default)
                  :facility => ::Syslog::Constants::LOG_USER, # optional syslog facility
                  :extra => {} # extra custom journal fields
        )
        Logging.mdc['LOGGROUP'] = group
      when 'syslog'
        result = Logging.appenders.syslog(group, :ident => "#{caller} -")
      when 'file'
        result = Logging.appenders.file(
          group, :filename => logfile, :layout => Logging.layouts.pattern(:pattern => "%d - %l : %c %m\n")
        )
      else
        raise "Unsupported log type: #{type}" unless screen
      end
      result
    end
  end
end
